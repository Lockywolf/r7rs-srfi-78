.PHONY: all
all: package
package: srfi-78.tgz
srfi-78.tgz:
	snow-chibi package "srfi/78.sld"

clean:
	rm -rf srfi-78.tgz
