;Time-stamp: <2019-10-26 22:01:35 lockywolf>
#;(Created 2019 10 26)
#;(Author Lockywolf)
#;(A simple interface to the examples of srfi-42)
(define-library (srfi 78 test)
  (export run-tests)
  (import (scheme base) (srfi 42)
	  (scheme r5rs) (srfi 78))
  (begin
    (define (run-tests)
      (include "srfi/78/examples.scm"))))
